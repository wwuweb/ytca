YouTube Caption Auditor (YTCA)
==============================

*YTCA* is a utility to collect stats from one or more YouTube channels.
It was designed to collect data related to captioning, but could be extended
to support other data collection needs.

Data Collected for each YouTube Channel
---------------------------------------

* Number of videos
* Number of videos with captions (does not include YouTube's machine-generated captions)
* Percent of videos that are captioned

Requirements
------------

* Python V3 or latest 
* A YouTube API key. For more information see the [YouTube Data API Reference][].


Instructions
------------

1. Get an API key from Google/Youtube.  If you're with Webtech, you can find the link and login in Confluence by searching under 'Youtube Caption Auditor'.
2. Create a file called 'apikey' and paste the key in there.
3. Add the google python api library to your environment. pip install --upgrade google-api-python-client
4. Run the application by passing in the channels listing ini file. $ python ytca.py --ini channels.ini
5. Alternatively, the application can also be run by using from the list of other commands: --cid, --username or --html and passing in appropriate 
arguments. More information about each command and its argument and the program output are explained below. 
Note: The program can only take one command and one argument

Example of program execution command and its arguments
-------------------

1. Run the program with ini file as argument: $ python ytca.py --ini channels.ini
2. Run the program with channel id (cid) as argument: $ python ytca.py --cid UCT845gB1xx0wrqfCIhUYeAg
3. Run the program with username as argument: $ $ python ytca.py --username usernamehere (Note: this command is outdated as explained in username section below)
4. Run the program to produce html file with channel ini as argument: $ python ytca.py --html channels.ini

YouTube Channel IDs
-------------------

The Configuration block includes an array of YouTube Channel IDs.
The YouTube Channel ID is a 24-character string that starts with the characters "UC".
This sometimes appears in the URL for the channel.
For example, the URL for the Western Washington University channel is

**https://www.youtube.com/channel/UCT845gB1xx0wrqfCIhUYeAg**

The channel ID is **UCT845gB1xx0wrqfCIhUYeAg**

If the channel URL is not presented in the above form, here's one way to
find the channel ID for that channel:

1. View source
2. Search the source code for the attribute *data-channel-external-id* or *"externalID":*
3. The value of that attribute is the channel ID

**While the channel URL format above is still valid, the youtube channel url had now change to
a @username handle**

So to use the example above, the URL for the main Western Washington University is
**https://www.youtube.com/@wwu**

To find the channel ID, use the three steps above

Note: Deprecated method below, use at your own risk
-------------------
**The username is now deprecated due to changes in Youtube API V3 which use channel ID
exclusively to identify youtube Channel and also due to certain feature from google plus that 
allow user from that to create account. This means that while some older usernames are 
regarded by Youtube as legacy feature that can still be use for channel identify. The 
majority of username now cannot be depended on to identify channel. Therefore, while the
username feature of this channel still work, it should not be use as a primary means to 
identify channels because it will likely tell you that a channel of this user doesn't exist 
when it actually does**

**Source: Google Developers Documentation** - **https://developers.google.com/youtube/v3/guides/working_with_channel_ids**

Alternatively, you can substitute the *user name* for any channel ID in the YTCA Configuration block.
As long as it's a valid user name and is not a 24-character string starting with "UC",
YTCA can look up the channel ID without you having to follow the above steps.

For example, here's an alternative URL for accessing channel using username:

**https://www.youtube.com/user/usernamehere**

The user name is **usernamehere**

In the YTCA Configuration block we can enter either **usernamehere** although the former adds overhead because YTCA needs to
lookup each unknown channel ID via an extra query to the YouTube Data API.

File Output
-------------------
1. Each option --ini, --cid or --username all produce a csv file ytcaReport_CURRENT_DATE.csv where CURRENT_DATE is replace with the system current date.
The csv file contains the same informations as the output from the terminal screen
2. Alternately, option --html can be use with a channel ini file can be pass in this program argument to produce a html file with the same name above 
but with .html extension. Example: ytcaReport_CURRENT_DATE.html 
3. All output options above produce a column headers table where each row is a channel element with each header column contain information pertain to 
each channel
4. If the file argument is incorrect for either --html or --ini option, the program will not display anything on the terminal and will 
produce an empty csv or empty html file.  

About Quotas
------------

The YouTube Data API has a limit on how many API queries can be executed per day.
For additional details, see [Quota usage][] in the API documentation.

Quotas can be estimated using YouTube's [Quota Calculator][].

As of June 2015, quota costs associated with running this application are:
* 100 units for each channel (search query)
* 5 units for each video
* 5 units for each channel ID lookup from a user name

For example, if you have a daily quota of 5,000,000 units you could run this application
to collect data from 100 channels (10,000 units) containing 998,000 videos.


[YouTube Data API Reference]: https://developers.google.com/youtube/v3/docs/
[Quota Usage]: https://developers.google.com/youtube/v3/getting-started#quota
[Quota Calculator]: https://developers.google.com/youtube/v3/determine_quota_cost

Credits:
------------
Terrill Thompson for creating this program, WebTech Employees past and present for curating the python version and David Nghiem for maintaining and fixing the bugs for this program
