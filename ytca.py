#!/usr/bin/python3
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError
from collections import OrderedDict
import time
import argparse
import csv
import os
try:
    import configparser
except ImportError:
    import ConfigParser as configparser

DEVELOPER_KEY = ""

with open('apikey', 'r') as f:
    DEVELOPER_KEY = f.readline()

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = DEVELOPER_KEY


YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

config = configparser.ConfigParser()

parser = argparse.ArgumentParser()
input_type_group = parser.add_mutually_exclusive_group()
input_type_group.add_argument(
    "--ini", help="Specify a ini file to process")
input_type_group.add_argument(
    "--username", help="Run the caption auditor on a single youtube account, by username")
input_type_group.add_argument(
    "--chid", help="Run the caption auditor on a single youtube account, by channel ID")
input_type_group.add_argument(
    "--html", help="Specify a ini file to process and output a html file")

# Initialize Data API object
youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                developerKey=DEVELOPER_KEY)

#Each channel is an object stored in the dictionary data structure, 
#each channel object utilized its internal methods to call youtube
#api to update and produce its statistic data
class Channel(object):
    def __init__(self, name, channel_id):
        self.name = name
        self.channel_id = channel_id
        self.uploads_list_id = ""
        self.videos_list = []
        self.total_videos = 0
        self.captioned_videos = 0

    @classmethod
    def from_username(cls, username):
        try:
            results = youtube.channels().list(
                part="snippet",
                forUsername=username
            ).execute()
        except HttpError as e:
            if e.status_code == 404:
                print("Category or channel not found...")
                exit()

        try:
            if (results['items']):
                items = results['items'][0]
                name = items['snippet']['title']
                channel_id = items['id']
                channel = cls(name, channel_id)
                return channel
        except:
            print("Error: YouTube username %s not found. Either syntax is incorrect or only channel Id will return the result" % username)
            print("Please use channel Id instead")
            exit()

    # Query the channel in question for the id of the uploads playlist
    def uploads_playlist(self):
        working = True
        try:
            results = youtube.channels().list(
                part="contentDetails",
                id=self.channel_id
            ).execute()
        except HttpError as e:
            if e.status_code == 404:
                print("Category or channel not found...")
                return not working
        
        try:
            channel = results["items"][0]
            if "contentDetails" in channel:
                uploads_list_id = channel['contentDetails']['relatedPlaylists']['uploads']
                self.uploads_list_id = uploads_list_id
            return working  
        except KeyError:
            print("Channel %s does not exist or its channel Id are incorrect \n" % self.name)
            return not working
        
    # Go through the uploads playlist and populate the list of video IDs
    def get_videos(self):
        playlist_results = youtube.playlistItems().list(
            playlistId=self.uploads_list_id,
            part="contentDetails",
            maxResults=50
        )

        # Iterate through all pages in result set
        print("Populating videos list...")
        while playlist_results:
            # try-catch to handle faulty execution
            try:
                response = playlist_results.execute()
            except HttpError as e:
                if e.status_code == 404:
                    print("Video not found from channel...")
                    break
            
            # Build list of video IDs to grab caption data from
            chunk = []

            if (response['items']):
                for playlist_item in response['items']:
                    video_id = playlist_item['contentDetails']['videoId']
                    chunk.append(video_id)

                self.total_videos += len(chunk)
                self.videos_list.append(chunk)

                 # Get the next page
                playlist_results = youtube.playlistItems().list_next(playlist_results, response)
            else: #if channel is empty
                break

    # Helper function for find_captions that
    def video_request(self, videos_str):
        video_response = youtube.videos().list(
            part="contentDetails",
            id=videos_str,
            maxResults=50
        ).execute()

        for video_result in video_response.get("items", []):
            if video_result['contentDetails']['caption'] == u'true':
                self.captioned_videos += 1

    def find_captions(self):
        print("Counting captioned videos...")
        for chunk in list(self.videos_list):
            # Turn video list into a comma separated string for the API query
            query_str = ",".join(chunk)
            self.video_request(query_str)
    
    def update_csv(self, filename, date):
        with open(filename, 'a', newline='') as f:
            writer = csv.writer(f)
            try:
                percentCaptioned = str(
                    round(((self.captioned_videos / self.total_videos)*100),2))
            except ZeroDivisionError:
                percentCaptioned = str(0)
            writer.writerow([date, self.name, self.total_videos,
                            self.captioned_videos, percentCaptioned])
    
    #add each table row with data about the channel
    def update_html(self, filename):
        try:
            percentCaptioned = str(
                round(((self.captioned_videos / self.total_videos)*100),2))
        except ZeroDivisionError:
                percentCaptioned = str(0)
        with open(filename, 'a') as f:
            html_template = """
                    <tr>
                        <th scope="row">""" + self.name + """</th>
                        <td>""" + str(self.total_videos) + """</td>
                        <td>""" + str(self.captioned_videos) + """</td>
                        <td>""" + str(percentCaptioned) + """</td>
                    </tr>
            """
            # writing the code into the file
            f.write(html_template)
            # close the file
            f.close()
    
    def run(self):
        print("Processing channel %s" % self.name)
        if(self.uploads_playlist()): #if channel actually exist then do works to get it
            self.get_videos()
            self.find_captions()
            print("Total videos in channel: %d" % self.total_videos)
            print("Captioned Videos: %d" % self.captioned_videos)
            try:
                print("Percent captioned: %0.2f%%\n" %
                  ((self.captioned_videos / self.total_videos)*100))
            except ZeroDivisionError:
                print("Percent captioned: 0.00%\n")


# -------------------------
def load_channels_list(filename):
    channels = OrderedDict()
    config.read(filename)
    for section in config._sections:
        name = config.get(section, "name")
        id = config.get(section, "id")
        if name.startswith('\''):
            name = name[1:-1]
        if id.startswith('\''):
            id = id[1:-1]
        channels.update({name: id})
    return channels


def create_csv(filename):
    with open(filename, 'w', newline='') as f:
        writer = csv.writer(f)
        writer.writerow(['Date', 'Channel Name', 'Total Videos',
                        'Captioned Videos', '% Captioned'])

#This function create upper half of the html file, including the open elements tag
def create_html(filename, today):
    with open(filename, 'w') as f:
        html_template = """<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <style>
            table, th, td {
                border:1px solid black;
            }

            h1,h2 {
                text-align: center;
            }
        </style>

        <title>YouTube Caption Auditor | """ + today + """ </title>
    </head>

    <body>
        <main>
            <h1>Youtube Data Result | """ + today + """</h1>

            <h2>This table contains statistics information about each Youtube channel including total number of videos and how much videos have captioned.</h2>
            <h2>Note: Captioned information only pertained to video that have manual caption, video with auto generated caption does not count.</h2>
            <table style="width:100%">
                <thead>
                    <caption>Column Headers provide information for each channel item in each row</caption>
                    <tr>
                        <th scope="col">Channel Name</th>
                        <th scope="col">Total Videos</th>
                        <th scope="col">Captioned Videos</th>
                        <th scope="col">% Captioned</th>
                    </tr>
                </thead>

                <tbody>       
        """
        # writing the code into the file
        f.write(html_template)
        # close the file
        f.close()

#This function produce the lower half the html file, with closing element tags and footer
def closing_html(filename): 
    with open(filename, 'a') as f:
        closing_template = """
                </tbody>
            </table>
        </main>
        
        <footer>
            <h2>Copyright 2023 WebTech Department of Western Washington University</h2>
        </footer>
    </body>
</html>
        """
        # writing the code into the file
        f.write(closing_template)
        # close the file
        f.close()

#After importing all the files, setting up parser and initialize Youtube API,
#the main function will be the first to run where arguments will be process
if __name__ == '__main__':
    today = time.strftime("%Y-%m-%d")
    filename = "ytcaReport_" + today + ".csv"

    args = parser.parse_args()

    #The arg.ini contain the file argument itself, ex: channels.ini
    if args.ini:
        create_csv(filename)
        channels = load_channels_list(args.ini)
        for name, id in channels.items():
            this_channel = Channel(name, id)
            this_channel.run()
            this_channel.update_csv(filename, today)
    
    elif args.chid: #The args.chid contain the channel id argument 
        create_csv(filename)
        channel = Channel(None, args.chid)
        channel.run()
        channel.update_csv(filename, today)
    
    elif args.username: #The args.username contain the username argument
        create_csv(filename)
        channel = Channel.from_username(args.username)
        channel.run()
        channel.update_csv(filename, today)

    # the args.html actually contains the filename that were pass in the argument
    # and is not a html file
    elif args.html: 
        htmlFile = "ytcaReport_" + today + ".html"
        create_html(htmlFile, today)
        channels = load_channels_list(args.html)
        for name, id in channels.items():
             this_channel = Channel(name, id)
             this_channel.run()
             this_channel.update_html(htmlFile)
        closing_html(htmlFile)
        
    else:
        parser.print_help()
